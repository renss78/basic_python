def print_stars_line_length_ten():
    print(10 * "*")

def print_stars_line(length):
    print(length * "*")


def main():
    line_length = 25
    print_stars_line_length_ten()
    print_stars_line(line_length)


if __name__ == "__main__":
    main()